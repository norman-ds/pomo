
<!-- README.md is generated from README.Rmd. Please edit that file  -->

# POMO

Das Bevölkerungsmonitoring (POMO) stellt Szenarien zur Entwicklung der
Bevölkerung in der Schweiz gegenüber. Das
[Beispiel](https://norman-ds.gitlab.io/pomo/) vergleicht drei Szenarien
(2014, 2019, 2020) des Kantons Zürichs.

<!-- badges: start -->

[![pipeline
status](https://gitlab.com/norman-ds/pomo/badges/main/pipeline.svg)](https://gitlab.com/norman-ds/pomo/-/commits/main)

<!-- badges: end -->

Die Bedeutung der erstellten Bevölkerungsprognosen nimmt in der Schweiz
weiter zu. Meist sind dafür Amtsstellen beim Bund, beim Kanton und bei
den Gemeinden zuständig.  
Bevölkerungsprognosen bleiben unsicher. Daher werden auch meist drei
Szenarien ausgearbeitet, wobei das Mittlere Szenario hauptsächlich für
weitere Berechnungen beigezogen wird.  
Die Verantwortlichen sind um eine Koordination bestrebt, verfolgen aber
auch partikular Interesse.

Bevölkerungsprognosen müssen periodisch überarbeitet und veröffentlicht
werden. [Bevölkerungsszenarien
Zürich](https://norman-ds.gitlab.io/pomo/)

## API

Die Website bietet eine Datenübersicht und stellt die Datensätze in
einer API im JSON- und original Format wieder zur Verfügung.

## Daten

Die Datensätze sind nicht einheitlich aufgebaut. Sie werden im Original
und als maschinenlesbare Version zur Verfügung gestellt.

## DevOps

POMO verfolgt einen kontinuierlichen Prozessansatz, der
Softwareentwicklung und IT-Betrieb abdeckt. Alle Scripts sind in
[R](https://www.r-project.org) geschrieben. Wobei für die
reproduzierbare Softwareentwicklung das Kontainersystem von
[Docker](https://hub.docker.com/r/rocker/verse) eingesetzt wird. Als
Versionierungssystem dient [GitLab](https://docs.gitlab.com/ee/ci/),
GitLab *Jobs* für CI/CD und GitLab *Page* als
[Webserver](https://norman-ds.gitlab.io/pomo/). Das Dashboard ist ein
HTMLwidget und verlangt serverseitig nur einen Standard Webserver.

Das IDE von RStudio wird in einem Docker gestartet und über den Browser
darauf zugegriffen
(<http://localhost:8787>).

``` yaml
docker run --name POMO -d -p 8787:8787 -v $(pwd):/home/rstudio -e PASSWORD=pwd rocker/verse:3.6.3
```

Das Docker Image von *rocker/verse* hat die R Version 3.6.3 und einige
Packages installiert und erspart uns somit ein Nachinstallieren.

``` r
sessionInfo()
#> R version 3.6.3 (2020-02-29)
#> Platform: x86_64-pc-linux-gnu (64-bit)
#> Running under: Debian GNU/Linux 10 (buster)
#> 
#> Matrix products: default
#> BLAS/LAPACK: /usr/lib/x86_64-linux-gnu/libopenblasp-r0.3.5.so
#> 
#> locale:
#>  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
#>  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
#>  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=C             
#>  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
#>  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
#> [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
#> 
#> attached base packages:
#> [1] stats     graphics  grDevices utils     datasets  methods   base     
#> 
#> loaded via a namespace (and not attached):
#>  [1] compiler_3.6.3  magrittr_1.5    tools_3.6.3     htmltools_0.4.0
#>  [5] yaml_2.2.1      Rcpp_1.0.4.6    stringi_1.4.6   rmarkdown_2.1  
#>  [9] knitr_1.28      stringr_1.4.0   xfun_0.13       digest_0.6.25  
#> [13] rlang_0.4.5     evaluate_0.14
```

Die Versions-Liste der verwendeten
R-Packages.

``` r
libs <- c("jsonlite","readr","readxl","dplyr","purrr","lubridate","flexdashboard","echarts4r", "glue", "htmltools")
ip <- installed.packages(fields = c("Package", "Version"))
ip <- ip[ip[,c("Package")] %in% libs,]
paste(ip[,c("Package")],ip[,c("Version")])
#>  [1] "dplyr 0.8.5"           "echarts4r 0.2.3"       "flexdashboard 0.5.1.1"
#>  [4] "glue 1.4.0"            "htmltools 0.4.0"       "jsonlite 1.6.1"       
#>  [7] "lubridate 1.7.8"       "purrr 0.3.4"           "readr 1.3.1"          
#> [10] "readxl 1.3.1"
```
