FROM rocker/verse:3.6.3

# Install R packages
## flexdashboard
RUN R -e "install.packages('flexdashboard', repos='https://cran.rstudio.com/')"
## dygraphs
RUN R -e "install.packages('echarts4r', repos='https://cran.rstudio.com/')"
## glue
RUN R -e "install.packages('glue', repos='https://cran.rstudio.com/')"
## htmltools
RUN R -e "install.packages('htmltools', repos='https://cran.rstudio.com/')"

