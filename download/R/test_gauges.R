#sourcelist <-  anybox$get()$json %>%
sourcelist <-  list.files("build", full.names = T) %>%
    set_names(gsub('\\..*$', '', basename(.))) %>%
  map(jsonlite::read_json, simplifyVector = T)

colnam <- c('Gender', 'A0to19', 'A20to39', 'A40to64', 'A65to79', 'A80up', 'A20to64', 'A65up', 'TT')

zh20 <- sourcelist$zh20$ZHInd$tables$kntabs %>%
  as_tibble() %>%
  filter(jahr==2030) %>%
  select(demGr, c(5:9, 11:13)) 

zh19 <- sourcelist$zh19$ZHInd$tables$kntabs %>%
  as_tibble()  %>%
  filter(Jahr==2030) %>%
  select(demGr, c(6:10, 12:14))
  
# build dataframe
mydata <- list(ZH2020=zh20,ZH2019=zh19) %>%
  map_dfr( ~ {names(.x) <- colnam; .x}, .id = 'Szenario') %>%
  mutate_if(is.double, as.integer) %>%
  pivot_longer(3:10, names_to = "Alter")


mydata %>%
  filter(Gender == 'TT') %>%
  pivot_wider(-Gender, names_from = Szenario) %>%
  mutate(abs = ZH2020-ZH2019, rel = round((ZH2020/ZH2019-1)*100,1))

mydata <-split(mydata, mydata$Gender) %>%
  map_dfr( ~ pivot_wider(.x, -Gender, names_from = Szenario)%>%
         mutate(abs = ZH2020-ZH2019, rel = round((ZH2020/ZH2019-1)*100,1)),
       .id = 'Gender') %>%
  filter(Alter %in% colnam[c(3,5:6,8)])

# pull a single val
filter(mydata, Gender== 'TT', Alter == 'A65up') %>%
  select(rel) %>%
  pull

# create a character vector 
filter(mydata, Gender== 'MT', Alter %in% c('A65to79', 'A80up')) %>%
  mutate(text = sprintf("%1.1f%% (%i)", rel, abs)) %>%
  select(Alter, text) %>%
  pivot_wider(names_from = Alter, values_from = text) %>%
  unlist
datatxt <- filter(mydata, Gender== 'FT', Alter %in% c('A65to79', 'A80up')) %>%
  mutate(text = sprintf("%1.1f%% (%i)", rel, abs)) %>%
  select(Alter, text) %>%
  pivot_wider(names_from = Alter, values_from = text) %>%
  unlist
caption <- sprintf("Im Szenario ZH2020 werden weniger Frauen mit Alter über 65 Jahren erwartet: 65 bis 79 : %s, 80 plus : %s", datatxt['A65to79'], datatxt['A80up'])
valueBox(datatxt['A80up'],
         caption = caption,
         icon = "fa-female",
         color =  "#FC363B")
